# step-projet

Студент №1 - **Дмитрий Беляк**

Студент №2 - **Олександра Гебеш**


Задание для студента №1

1. Сверстать шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана).
2. Сверстать секцию ***"People Are Talking About Fork"***.

Задание для студента №2

1. Сверстать блок ***"Revolutionary Editor"***.
2. Светстать секцию ***"Here is what you get"***.
3. Сверстать секцию ***"Fork Subscription Pricing"***.
